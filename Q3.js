//creating variables
idNum = 0
customerName = ""
unitInput = 0
electricity = 0

//creating console log title
console.log("Customer Electricity Bill calculator")
console.log("------------------------------------")


//creating prompts so that user can input ID, Name and Units (adding making sure it is a number for only ID and units), and showing inputs in console.log
idNum = Number(prompt("Welcome to electricity bill calculator. Please enter customer ID number:"))
console.log(`Customer ID Number: ${idNum}`)

customerName = prompt("Please Enter your name:")
console.log(`Customer Name: ${customerName}`)

unitInput = Number(prompt(`Please enter the number of units ${customerName} has used:`))
console.log(`Units: ${unitInput}`)


console.log ("------------------------------------")


//creating if statements to determine what rate to charge the customer, and calculating electricity bill (and rounding to 2 d.p.)
if (unitInput < 200){

    electricity = (unitInput*1.20).toFixed(2)
    console.log(`The total owing at a rate of $1.20 is: $${electricity}`)

}


else if (unitInput>=200 && unitInput<400) {

    electricity = (unitInput*1.50).toFixed(2)
    console.log(`The total owing at a rate of $1.50 is: $${electricity}`)

}

else if (unitInput>=400 && unitInput<600) {

    electricity = (unitInput*1.80).toFixed(2)
    console.log(`The total owing at a rate of $1.80 is: $${electricity}`)

}

else if ( unitInput>=600) {

    electricity = (unitInput*2.00).toFixed(2)
    console.log(`The total owing at a rate of $2.00 per unit is: $${electricity}`)

}

else {
    console.log("Invalid Input")
}