//creating variables
fahrenheit = 0
celsiusInput = 0

//creating prompt for user to enter in temperature (and making sure it is a number)
celsiusInput = Number(prompt("Welcome to temperature converter.\nEnter temperature in degrees Celsius to convert to degrees Fahrenheit:"))

//creating title for the application
console.log("Temperature Converter")
console.log("---------------------")

//converting temperature

fahrenheit = (((celsiusInput*18))/(10)+32).toFixed(1)

//outputting result to console log
console.log(`${celsiusInput} degrees Celsius is equal to ${fahrenheit} degrees fahrenheit`)