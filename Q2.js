//creating variables
userInput = 0
average = 0
total = 0


//creating title for console log
console.log("Average Calculator")
console.log("------------------")


//creating loop to allow user to input 10 numbers (and make sure it is a number)
for(i=1; i<=10; i++){

    userInput = Number(prompt(`Please enter number ${i}`))

    //writing the input numbers to console log
    console.log(`Number ${i}: ${userInput}`)

    //adding numbers as each number is inputted
    total = total + userInput
}

//calculating average 
average = (total/10)

//outputting average
console.log(`The average of those 10 numbers is ${average}`)